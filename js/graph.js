  // Set up zoom support
  var svg = d3.select("svg"),
      inner = svg.select("g"),
      zoom = d3.zoom().on("zoom", function() {
        inner.attr("transform", d3.event.transform);
      });
  svg.call(zoom);

  var render = new dagreD3.render();

  // Left-to-right layout
  var g = new dagreD3.graphlib.Graph();
  g.setGraph({
    nodesep: 70,
    ranksep: 50,
    rankdir: "LR",
    marginx: 20,
    marginy: 20
  });

  var styleTooltip = function(name, description) {
    return "<p class='name'>" + name + "</p><p class='description'>" + description + "</p>";
  };


  function draw(isUpdate) {
    for (var id in actors) {
      var actor = actors[id];
      var className = "tooltip " + actor.type;
      var html = "<div>";
      html += "<span class=status></span>";
      html += "<span class=name>"+id+"</span>";
      html += "<span class=\"tooltiptext\">"+actor.description+"</span>"
      html += "</div>";
      g.setNode(id, {
        labelType: "html",
        label: html,
        rx: 5,
        ry: 5,
        padding: 0,
        class: className
      });

      // build the network
      if (actor.inputQueue) {
        for (var i=0; i<actor.inputQueue.length; i++) {
          g.setEdge(actor.inputQueue[i].name, id, {
            label: actor.inputQueue[i].label,
            width: actor.inputQueue[i].width
            /*,
            weight: actor.inputQueue[i].weight
            */
          });
        }
      }
    }

    inner.call(render, g);

    // Zoom and scale to fit
    var graphWidth = g.graph().width + 80;
    var graphHeight = g.graph().height + 40;
    var width = parseInt(svg.style("width").replace(/px/, ""));
    var height = parseInt(svg.style("height").replace(/px/, ""));
    var zoomScale = Math.min(width / graphWidth, height / graphHeight);
    var translateX = (width / 2) - ((graphWidth * zoomScale) / 2)
    var translateY = (height / 2) - ((graphHeight * zoomScale) / 2);
    var svgZoom = isUpdate ? svg.transition().duration(500) : svg;
    //svgZoom.call(zoom.transform, d3.zoomIdentity.translate(translateX, translateY).scale(zoomScale));
  }

  // Do some mock queue status updates
  setInterval(function() {
    //var stoppedWorker1Count = actors["elasticsearch-writer"].count;
    //var stoppedWorker2Count = actors["meta-enricher"].count;
    //for (var id in actors) {
     // actors[id].count = Math.ceil(Math.random() * 3);
    //  if (actors[id].inputThroughput) actors[id].inputThroughput = Math.ceil(Math.random() * 250);
    //}
    //actors["elasticsearch-writer"].count = stoppedWorker1Count + Math.ceil(Math.random() * 100);
    //actors["meta-enricher"].count = stoppedWorker2Count + Math.ceil(Math.random() * 100);
    draw(true);
  }, 1000);

  // Do a mock change of actor configuration
  setInterval(function() {
    actors["elasticsearch-monitor"] = {
      //"consumers": 0,
      "count": 0,
      "inputQueue": "elasticsearch-writer",
      "inputThroughput": 50
    }
  }, 5000);

  // Initial draw, once the DOM is ready
  document.addEventListener("DOMContentLoaded", draw);

  inner.selectAll("g.node")
    .attr("title", function(v) { return styleTooltip(v, g.node(v).description) })
    .each(function(v) { $(this).tipsy({ gravity: "w", opacity: 1, html: true }); });
